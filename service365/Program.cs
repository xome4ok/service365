﻿using System.ServiceProcess;


namespace service365
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new svip() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
