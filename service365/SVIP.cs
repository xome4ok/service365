using System;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Timers;
using System.Management;
using System.IO;

namespace service365
{
    public partial class svip : ServiceBase
    {
        private const string filename = "app365.exe";
        private const string arguments = "";
        int pid; //id of process and session under control
        bool isRunning;
        private const string logSource = "SVIP_365";
        private Timer timer;

        public svip()
        {
            try { if (!EventLog.SourceExists(logSource)) EventLog.CreateEventSource(logSource, "SVIP"); } //instantinate logging 
            catch (ArgumentException) { EventLog.WriteEntry("SVIP service warning", "Service Error on start: Cannot create log"); }

            var cwd = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);
            Directory.SetCurrentDirectory(cwd ?? ".");

            isRunning = false;

            CanHandleSessionChangeEvent = true;

            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                CheckExistance(true);
                StartTimer();
            }
            catch (Exception e) {  EventLog.WriteEntry(logSource, "Service Error on start: " + e);  }
        }

        private void StartTimer()
        {
            timer = new Timer { Interval = 3000, Enabled = true };
            timer.Elapsed += (sender, args) => CheckExistance();
        }
        private void CheckExistance(bool onStart = false)
        {
            try
            {
                //EventLog.WriteEntry(logSource, "checking for existance of process with pid" + pid);
                isRunning = !onStart && IsRunning(pid);
                if (isRunning) return;
                //EventLog.WriteEntry(logSource, onStart ? "Starting application." : "App with pid " + pid + "was closed.");
                StartProcess();
            }

            catch (Exception e) {  EventLog.WriteEntry(logSource, "Service Error in check: " + e);  }
        }

        private void StartProcess()
        {
            try
            {
                var process = new Process
                {
                    StartInfo = new ProcessStartInfo { FileName = ".\\" + filename, Arguments = arguments }
                };
                pid = process.StartAsActiveUser();
                isRunning = true;
            }
            catch(System.ComponentModel.Win32Exception)
            {
                //common thing. dunno what really useful to do here
                
                //EventLog.WriteEntry(logSource, "Somthing gone wrong in service: " + e);
            }
        }

        private bool IsRunning(int pid)
        {
            var mgmtClass = new ManagementClass("Win32_Process");
            var result = false;
            foreach (var mgmtObj in mgmtClass.GetInstances())
            {
                //int sid = -1;
                //try { sid = (Process.GetProcessById(pid)).SessionId; } catch(ArgumentException) {}
                
                ////if current sessionId doesn't match the stored one
                //if (sid != -1 && Convert.ToInt32(mgmtObj["SessionId"]) != sid)
                //{
                //    KillApp();
                //    return false;
                //}
                
                if (Convert.ToInt32(mgmtObj["ProcessId"]) == pid)
                {
                    //System.Diagnostics.EventLog.WriteEntry("SVIP365 Debug", "Found running app365 with pid: " + pid);//+ " sid: " + sid);
                    result = true;
                }
            }
            return result;
        }

        private void KillApp()
        {
            timer.Stop();
            timer.Dispose();
            foreach (var process in Process.GetProcessesByName("app365").Where(process => !process.CloseMainWindow()))
            {
                process.Kill();
                //System.Diagnostics.EventLog.WriteEntry("SVIP365 Debug", "Killed process with pid " + process.Id);
            }
            isRunning = false;
        }

        protected override void OnStop()
        {
            KillApp();
        }

        protected override void OnSessionChange(SessionChangeDescription changeDescription)
        {
            //EventLog.WriteEntry(logSource, "Session<X> Event fired. " + changeDescription.Reason);
            switch (changeDescription.Reason)
            {
                case SessionChangeReason.ConsoleConnect:
                    KillApp();
                    CheckExistance();
                    StartTimer();
                    break;
                case SessionChangeReason.ConsoleDisconnect:
                    KillApp();
                    break;

                //case SessionChangeReason.SessionLock:
                //    KillApp();
                //    break;
                //case SessionChangeReason.SessionUnlock:
                //    KillApp();
                //    CheckExistance();
                //    StartTimer();
                //    break;
                //case SessionChangeReason.SessionLogoff:
                //    KillApp();
                //    break;
                //case SessionChangeReason.SessionLogon:
                //    KillApp();
                //    CheckExistance();
                //    StartTimer();
                //    break;
            }

            base.OnSessionChange(changeDescription);
        }


    }
}
