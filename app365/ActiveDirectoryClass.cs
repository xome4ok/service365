﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Windows;
using NLog;

namespace app365
{
    public class ActiveDirectoryClass
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static List<string> GetUserInfo()
        {
            var userinfo = new List<string>();
            try
            {
                var uname = Environment.UserName;
                // System.Diagnostics.EventLog.WriteEntry("SVIP debug", "username=" + uname);
                var defaultNamingContext = new DirectoryEntry("LDAP://RootDSE").Properties["defaultNamingContext"].Value.ToString();
                // System.Diagnostics.EventLog.WriteEntry("SVIP debug", "defaultNamingContext="+defaultNamingContext);
                using (var de = new DirectoryEntry("LDAP://" + defaultNamingContext))
                {
                    using (var adSearch = new DirectorySearcher(de))
                    {
                        adSearch.Filter = String.Format("(sAMAccountName={0})", uname);
                        var adSearchResult = adSearch.FindOne();
                        var properties = adSearchResult.Properties;

                        var pager = properties["pager"]; //pager field is used to determine whether to run application.
                        // if pager is not empty we run app
                        var displayName = properties["displayName"]; // -> name
                        var description = properties["department"]; // -> position
                        var title = properties["company"]; // -> jobTitle
                        var info = properties["title"]; // -> profession
                        var syncFolder = properties["facsimileTelephoneNumber"]; // -> remote folder where media for current user has to be stored

                        userinfo.Add((pager.Count != 0) ? "run" : "");//really don't care what exactly was in this field
                        userinfo.AddRange((new[] { displayName, description, info, title, syncFolder })
                            .Select(attr => (attr.Count != 0) ? attr[0].ToString() : "")); // empty [0] indicates that AD is not needed to be run for this user.
                    }
                }
            }
            catch (ArgumentOutOfRangeException e)
            {

                //System.Diagnostics.EventLog.WriteEntry("SVIP365", "App Error: incorrect AD data.\n" + e);
                logger.Error("Incorrect data in ActiveDirectory. Stack trace: {0} {1}", e.StackTrace, e.ToString());
                MessageBox.Show("Некорректно заполнены поля в Active Directory.", "SVIP - Ошибка!");

                Application.Current.Shutdown();

            }
            catch (System.Runtime.InteropServices.COMException e)
            {
                //System.Diagnostics.EventLog.WriteEntry("SVIP365", "App Error: Perhaps can't log in to the AD domain.");
                logger.Error("Can't log into ActiveDirectory domain. Stack trace: {0} {1}", e.StackTrace, e.ToString());
                MessageBox.Show("Невозможно войти в домен.", "SVIP - Ошибка!");

                Application.Current.Shutdown();
            }
            catch (Exception e)
            {
                //System.Diagnostics.EventLog.WriteEntry("SVIP365", "App Error:  Unknown error.\n" + e);
                logger.Error("Impossible unknown error. Stack trace: {0} {1}", e.StackTrace, e.ToString());
                MessageBox.Show("Неизвестная ошибка.", "SVIP - Ошибка!");

                Application.Current.Shutdown();
            }
            return userinfo;
        }

    }
}
