﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms;


namespace app365
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        public App() : base()
        {
            QuickConverter.EquationTokenizer.AddNamespace(typeof(object));
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {

            //messing with AD

            var args = Environment.GetCommandLineArgs();
            var disableADkey = args.Length > 1 ? args[1] : "enable";

            bool enableAD = disableADkey == "--noad" ? false : true;

            var needRun = false;

            if (enableAD)
            {
                var info = ActiveDirectoryClass.GetUserInfo();
                try { needRun = info[0] != ""; } // if info is not empty then we need to run the app.
                catch (ArgumentOutOfRangeException) { return; }// it usually means that info from AD was incorrect or connection to AD is lost 
            }
            else
            {
                needRun = true;
            }

            Screen target = null;
            foreach (var s in Screen.AllScreens.Where(s => s != Screen.PrimaryScreen))
            {
                target = s; //switch to this screen

                MoveAllWindowsButCurrent(Screen.PrimaryScreen.WorkingArea.Left, Screen.PrimaryScreen.WorkingArea.Top);

                if (needRun)
                {
                    var iw = new InfoWindow
                    {
                        Top = target.WorkingArea.Top,
                        Left = target.WorkingArea.Left,
                        Width = target.WorkingArea.Width,
                        Height = target.WorkingArea.Height
                    };

                    iw.Show(); //show the window on it

                    iw.Visibility = Visibility.Collapsed; // cover the taskbar
                    iw.WindowState = System.Windows.WindowState.Maximized;
                    iw.WindowStyle = WindowStyle.None;
                    iw.Visibility = Visibility.Visible;
                }
            }

            if (target != null || !needRun) { return; }

            System.Windows.MessageBox.Show("Дополнительный монитор не подключен. Подключите его, чтобы продолжить работу.", "SVIP");
            System.Windows.Application.Current.Shutdown();
        }

        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        private static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);

        [DllImport("USER32.DLL")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        //move all windows but our app to primary screen
        public static void MoveAllWindowsButCurrent(int newX, int newY)
        {
            //const short SWP_NOMOVE = 0X2;
            const short SWP_NOSIZE = 1;
            const short SWP_NOZORDER = 0X4;
            const int SWP_SHOWWINDOW = 0x0040;
            const int SW_SHOWNORMAL = 1; // restore window from maximized state

            Process[] processes = Process.GetProcesses(".");
            // uncomment the following line and .Where statement to avoid moving current proccess' window

             var thisProcess = Process.GetCurrentProcess();

            //move all windows. however it won't move explorer windows, cause they're the part of explorer.exe process, and not it's MainWindow
            foreach (var process in processes.Where(p => !thisProcess.Equals(p)))
            {
                IntPtr handle = process.MainWindowHandle;
                if (handle != IntPtr.Zero)
                {
                    ShowWindow(handle, SW_SHOWNORMAL);
                    SetWindowPos(handle, 0, newX, newY, 0, 0, SWP_NOZORDER | SWP_NOSIZE | SWP_SHOWWINDOW);
                }
            }

            //move explorer.exe related windows
            string filename;
            foreach (SHDocVw.InternetExplorer window in new SHDocVw.ShellWindows())
            {
                filename = Path.GetFileNameWithoutExtension(window.FullName).ToLower();
                if (filename.ToLowerInvariant() == "explorer")
                {
                    window.Left = newX;
                    window.Top = newY;
                }
            }
        }

    }
}
