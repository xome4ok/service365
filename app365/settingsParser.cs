﻿using NLog;
using System;
using System.Drawing;
using System.Xml;

namespace app365
{

    public struct Settings
    {
        #region variables
        public string allowStatus, denyStatus, greetingText, beforePositionText;
        public System.Windows.Media.Color allowColor, denyColor, statusFontColor;
        public Font nameFont, positionFont, statusFont, greetingFont, professionFont, jobTitleFont;
        public int greetingMargin, positionMargin, nameMargin, professionMargin, jobTitleMargin, statusMargin;

        //added in 2.0
        public bool isScrollTextEnabled;
        public string pathToScrollTextFile, pathToLocalMediaFolder;
        public TimeSpan timeToShowInfo;
        public Font scrollTextFont;
        public System.Windows.Media.Color scrollTextColor;

        //added in 2.4
        public TimeSpan timeToShowVideo;
        #endregion

        public Settings(string allowStatus,
            string denyStatus,
            System.Windows.Media.Color allowColor,
            System.Windows.Media.Color denyColor,
            Font nameFont,
            Font positionFont,
            Font statusFont,
            System.Windows.Media.Color statusFontColor,
            string greetingText,
            Font greetingFont,
            string beforePositionText,
            Font professionFont,
            Font jobTitleFont,
            int greetingMargin,
            int positionMargin,
            int nameMargin,
            int professionMargin,
            int jobTitleMargin,
            int statusMargin,
            bool isScrollTextEnabled,
            string pathToScrollTextFile,
            string pathToLocalMediaFolder,
            TimeSpan timeToShowInfo,
            Font scrollTextFont,
            System.Windows.Media.Color scrollTextColor,
            TimeSpan timeToShowVideo
            )
        {
            #region status init
            this.allowStatus = allowStatus;
            this.denyStatus = denyStatus;
            this.allowColor = allowColor;
            this.denyColor = denyColor;
            this.statusMargin = statusMargin;
            #endregion

            #region information fields init
            this.nameFont = nameFont;
            this.positionFont = positionFont;

            this.statusFont = statusFont;
            this.statusFontColor = statusFontColor;

            this.greetingText = greetingText;
            this.greetingFont = greetingFont;

            this.beforePositionText = beforePositionText;

            this.professionFont = professionFont;
            this.jobTitleFont = jobTitleFont;

            this.greetingMargin = greetingMargin;
            this.positionMargin = positionMargin;
            this.nameMargin = nameMargin;
            this.professionMargin = professionMargin;
            this.jobTitleMargin = jobTitleMargin;
            #endregion

            #region media and scroll text fields init. since 2.0
            this.isScrollTextEnabled = isScrollTextEnabled;
            this.pathToLocalMediaFolder = pathToLocalMediaFolder;
            this.pathToScrollTextFile = pathToScrollTextFile;
            this.timeToShowInfo = timeToShowInfo;
            this.scrollTextFont = scrollTextFont;
            this.scrollTextColor = scrollTextColor;

            this.timeToShowVideo = timeToShowVideo;
            #endregion
        }
        public override string ToString()
        {
            var str = " allowStatus: " +
            this.allowStatus +
            " denyStatus: " +
            this.denyStatus +
            " allowColor: " +
            this.allowColor +
            " denyColor: " +
            this.denyColor +
            " statusMargin: " +
            this.statusMargin +
            " nameFont: " +
            this.nameFont +
            " positionFont: " +
            this.positionFont +
            " statusFont: " +
            this.statusFont +
            " statusFontColor: " +
            this.statusFontColor +
            " greetingText: " +
            this.greetingText +
            " greetingFont: " +
            this.greetingFont +
            " beforePositionText: " +
            this.beforePositionText +
            " professionFont: " +
            this.professionFont +
            " jobTitleFont: " +
            this.jobTitleFont +
            " greetingMargin: " +
            this.greetingMargin +
            " positionMargin: " +
            this.positionMargin +
            " nameMargin: " +
            this.nameMargin +
            " professionMargin: " +
            this.professionMargin +
            " jobTitleMargin: " +
            this.jobTitleMargin +
            " isScrollTextEnabled: " +
            this.isScrollTextEnabled +
            " pathToLocalMediaFolder: " +
            this.pathToLocalMediaFolder +
            " pathToScrollTextFile: " +
            this.pathToScrollTextFile +
            " timeToShowInfo: " +
            this.timeToShowInfo +
            " scrollTextFont: " +
            this.scrollTextFont +
            " scrollTextColor: " +
            this.scrollTextColor +
            " timeToShowVideo: " +
            this.timeToShowVideo;

            return str;
        }
    }

    class SettingsParser
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public static Settings Parse(string filename)
        {
            var settingsXml = new XmlDocument();
            try
            {
                settingsXml.Load(filename);

                #region status
                var AS = settingsXml.DocumentElement.SelectSingleNode("/Settings/AllowPrefs/Status").InnerText;
                var DS = settingsXml.DocumentElement.SelectSingleNode("/Settings/DenyPrefs/Status").InnerText;
                var AC = settingsXml.DocumentElement.SelectSingleNode("/Settings/AllowPrefs/StripeColor").InnerText;
                var DC = settingsXml.DocumentElement.SelectSingleNode("/Settings/DenyPrefs/StripeColor").InnerText;
                var SM = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Status/Margin").InnerText;
                #endregion

                #region information
                var NFsize = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Name/FontSize").InnerText;
                var NFfamily = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Name/Font").InnerText;
                var PFsize = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Position/FontSize").InnerText;
                var PFfamily = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Position/Font").InnerText;
                var SFsize = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Status/FontSize").InnerText;
                var SFfamily = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Status/Font").InnerText;
                var SFC = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Status/Color").InnerText;
                var GFsize = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Greeting/FontSize").InnerText;
                var GFfamily = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Greeting/Font").InnerText;
                var JTFsize = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/JobTitle/FontSize").InnerText;
                var JTFfamily = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/JobTitle/Font").InnerText;
                var ProFsize = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Profession/FontSize").InnerText;
                var ProFfamily = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Profession/Font").InnerText;
                var GT = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Greeting/Text").InnerText;
                var BPT = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Position/Text").InnerText;
                var greetM = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Greeting/Margin").InnerText;
                var posM = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Position/Margin").InnerText;
                var nameM = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Name/Margin").InnerText;
                var profM = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/Profession/Margin").InnerText;
                var jobM = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/JobTitle/Margin").InnerText;
                #endregion

                #region scroll text and media
                var _scrollTextEnabled = settingsXml.DocumentElement.SelectSingleNode("/Settings/ScrollTextPrefs/Enabled").InnerText;
                var _pathToScrollTextFile = settingsXml.DocumentElement.SelectSingleNode("/Settings/ScrollTextPrefs/RelativePathToFile").InnerText;
                var _pathToLocalMediaFolder = settingsXml.DocumentElement.SelectSingleNode("/Settings/MediaPrefs/PathToLocalMediaFolder").InnerText;
                var _timeToShowInfoMins = settingsXml.DocumentElement.SelectSingleNode("/Settings/MediaPrefs/TimeToShowInfo/Min").InnerText;
                var _timeToShowInfoSecs = settingsXml.DocumentElement.SelectSingleNode("/Settings/MediaPrefs/TimeToShowInfo/Sec").InnerText;
                var _scrollTextFontFamily = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/ScrollText/Font").InnerText;
                var _scrollTextFontFamilySize = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/ScrollText/FontSize").InnerText;
                var _scrollTextColor = settingsXml.DocumentElement.SelectSingleNode("/Settings/TextPrefs/ScrollText/Color").InnerText;
                var _timeToShowVideoMins = settingsXml.DocumentElement.SelectSingleNode("/Settings/MediaPrefs/TimeToShowVideo/Min").InnerText;
                var _timeToShowVideoSecs = settingsXml.DocumentElement.SelectSingleNode("/Settings/MediaPrefs/TimeToShowVideo/Sec").InnerText;
                #endregion

                var parsed = new Settings(
                    allowStatus: AS,
                    denyStatus: DS,
                    allowColor: (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(AC),
                    denyColor: (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(DC),
                    nameFont: new Font(NFfamily, Convert.ToInt32(NFsize)),
                    positionFont: new Font(PFfamily, Convert.ToInt32(PFsize)),
                    statusFont: new Font(SFfamily, Convert.ToInt32(SFsize)),
                    statusFontColor: (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(SFC),
                    greetingText: GT,
                    greetingFont: new Font(GFfamily, Convert.ToInt32(GFsize)),
                    beforePositionText: BPT,
                    professionFont: new Font(ProFfamily, Convert.ToInt32(ProFsize)),
                    jobTitleFont: new Font(JTFfamily, Convert.ToInt32(JTFsize)),
                    greetingMargin: Convert.ToInt32(greetM),
                    positionMargin: Convert.ToInt32(posM),
                    nameMargin: Convert.ToInt32(nameM),
                    professionMargin: Convert.ToInt32(profM),
                    jobTitleMargin: Convert.ToInt32(jobM),
                    statusMargin: Convert.ToInt32(SM),
                    isScrollTextEnabled: Convert.ToBoolean(_scrollTextEnabled),
                    pathToScrollTextFile: _pathToScrollTextFile,
                    pathToLocalMediaFolder: _pathToLocalMediaFolder,
                    timeToShowInfo: new TimeSpan(0, Convert.ToInt32(_timeToShowInfoMins), Convert.ToInt32(_timeToShowInfoSecs)),
                    scrollTextFont: new Font(_scrollTextFontFamily, Convert.ToInt32(_scrollTextFontFamilySize)),
                    scrollTextColor: (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(_scrollTextColor),
                    timeToShowVideo: new TimeSpan(0, Convert.ToInt32(_timeToShowVideoMins), Convert.ToInt32(_timeToShowVideoSecs))
                    );
                return parsed;
            }
            catch (Exception e)
            { //if something goes wrong, just use default values
                // System.Diagnostics.EventLog.WriteEntry("SVIP365", "App Error: " + e + "\n using default values");
                logger.Error("Incorrect settings file, using defaults. Stack trace:\n{0} {1}",e.StackTrace, e.ToString());
                System.Windows.MessageBox.Show("Файл настроек некорректен. Обратитесь к системному администратору.", "SVIP");
                //System.Windows.MessageBox.Show(e.ToString() + "\n\n" + e.StackTrace, "SVIP");
                return new Settings(
                    allowStatus: "Свободно",
                    denyStatus: "Занято",
                    allowColor: System.Windows.Media.Colors.Green,
                    denyColor: System.Windows.Media.Colors.Red,
                    nameFont: new Font("Arial", 20),
                    positionFont: new Font("Arial", 14),
                    statusFont: new Font("Arial", 24),
                    statusFontColor: System.Windows.Media.Colors.White,
                    greetingText: "Уважаемые пациенты, ",
                    greetingFont: new Font("Arial", 14),
                    beforePositionText: "Прием ведет ",
                    professionFont: new Font("Arial", 14),
                    jobTitleFont: new Font("Arial", 14),
                    greetingMargin: 0,
                    positionMargin: 0,
                    nameMargin: 30,
                    professionMargin: 30,
                    jobTitleMargin: 0,
                    statusMargin: 220,
                    isScrollTextEnabled: false,
                    pathToScrollTextFile: ".\\media\\text.txt",
                    pathToLocalMediaFolder: ".\\media\\",
                    timeToShowInfo: new TimeSpan(0, 0, 10),
                    scrollTextFont: new Font("Arial", 10),
                    scrollTextColor: System.Windows.Media.Colors.White,
                    timeToShowVideo: new TimeSpan(0,0,10)
                    );
            }
        }
    }
}
