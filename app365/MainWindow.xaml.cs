﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Forms;
using System.Windows.Interop;
using Application = System.Windows.Application;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using NLog;
using Microsoft.Synchronization.Files;
using Microsoft.Synchronization;
using System.Linq;
using System.Windows.Media.Imaging;

namespace app365
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class InfoWindow : Window
    {
        #region locals
        private static Logger logger = LogManager.GetCurrentClassLogger();

        bool status;
        private const string settingsFile = "./settings.xml";
        string name = "",
            position = "",
            profession = "",
            jobTitle = "",
            greeting = "",
            reception = "",
            statusText = "";
        NotifyIcon tray = new NotifyIcon();
        Settings sett;
        System.Drawing.Icon allowIcon, denyIcon;

        bool mediaModeOn = false;
        List<Uri> mediaList = new List<Uri>();
        int currentMediaNumberInList = 0;

        Timer timer = new Timer();

        List<string> allowedExtensions = new List<string> { ".avi", ".mpeg4", ".jpeg", ".png", ".jpg", ".mpg", ".mov", ".bmp", ".wmv", ".mkv", ".mpeg", ".gif" };

        // this variable is for debug. Set it to false if you want build without syncronization feature
        bool isSyncEnabled = true;

        string scrollText;

        ImageBrush backgroundImage = new ImageBrush(new BitmapImage(new Uri("images/background.jpg", UriKind.Relative)));
        Brush black = new SolidColorBrush(Colors.Black);
        #endregion

        public InfoWindow()
        {
            #region cli args for disabling AD
            var args = Environment.GetCommandLineArgs();
            var disableADkey = args.Length > 1 ? args[1] : "enable";

            bool enableAD = disableADkey == "--noad" ? false : true;
            #endregion

            #region initializing info
            List<string> info = null;

            if (enableAD)
            {
                info = ActiveDirectoryClass.GetUserInfo();
            }
            else
            {
                //TODO: debug on win7. doesn't work for some reason.. i suspect 64bit library.
                var remoteDirTest = "\\\\VBOXSVR\\video_test";//"\\\\LENOVO\\Users\\Администратор\\Videos";
                info = new List<string> { "test", "test_name", "test_position", "test_profession", "test_jobTitle", remoteDirTest };
            }
            #endregion

            try
            {

                #region loading resources and settings
                logger.Trace("loading resources...");
                allowIcon = new System.Drawing.Icon("./images/online.ico");
                denyIcon = new System.Drawing.Icon("./images/busy.ico");

                //get settings from xml file
                logger.Trace("parsing settings from settings.xml ...");
                sett = SettingsParser.Parse(settingsFile);

                if (sett.allowStatus.Length > 30 || sett.denyStatus.Length > 30)
                    System.Windows.MessageBox.Show("Длина статуса не может превышать 30 символов. Статус будет показан не полностью.", "SVIP - Внимание!");
                logger.Trace("Resources successfully loaded");
                logger.Trace("Parsed settings: " + sett.ToString());
                #endregion

                #region initializing texts
                logger.Trace("initializing texts from AD...");
                greeting = sett.greetingText;
                reception = sett.beforePositionText;
                name = info[1];
                position = info[2];
                profession = info[3];
                jobTitle = info[4];
                #endregion

                logger.Trace("Initializing window components...");

                InitializeComponent();

                DataContext = this;

                #region media folder syncing
                if (isSyncEnabled)
                {
                    logger.Trace("Starting syncronization...");
                    logger.Trace("Directory {0} exists? Answer is: {1}", info[5], Directory.Exists(info[5]));
                    if (Directory.Exists(info[5]))
                    {
                        if (Directory.Exists(sett.pathToLocalMediaFolder))
                        {
                            logger.Trace("Both directories exist. Next action would be syncing...");

                            SyncFolders(info[5], sett.pathToLocalMediaFolder);

                        }
                        else
                        {
                            logger.Warn("Local directory {0} is either incorrect or doesn't exist. Aborting syncronization.", sett.pathToLocalMediaFolder);
                        }

                    }
                    else
                    {
                        logger.Warn("Remote directory {0} is either incorrect or doesn't exist. Aborting syncronization.", info[5]);
                    }
                }
                else
                {
                    logger.Trace("Syncronization is disabled in this build, skipping it...");
                    logger.Trace("Text in ActiveDirectory's Fax filed says: {0}", info[5]);
                }

                #endregion

                #region init text style and formatting for main info
                logger.Trace("Applying formatting and styles...");
                StyleInit(statusTB, sett.statusFont.FontFamily.Name, sett.statusFont.Size, 0);
                statusTB.Foreground = new SolidColorBrush(sett.statusFontColor);

                StyleInit(nameTB, sett.nameFont.FontFamily.Name, sett.nameFont.Size, sett.nameMargin);

                StyleInit(posTB, sett.positionFont.FontFamily.Name, sett.positionFont.Size, sett.positionMargin);

                StyleInit(greetingLABEL, sett.greetingFont.Name, sett.greetingFont.Size, sett.greetingMargin);

                StyleInit(profTB, sett.professionFont.Name, sett.jobTitleFont.Size, sett.professionMargin);

                StyleInit(jobTitleTB, sett.jobTitleFont.Name, sett.professionFont.Size, sett.jobTitleMargin);

                #endregion

                #region status and tray icon initializing
                //initialize status
                logger.Trace("Setting status to false...");
                SetStatus(false);

                // initialize tray context menu
                logger.Trace("Making tray icon up and running...");
                tray.Visible = false;

                var trayContextMenu = new ContextMenu();
                var allowMenuItem = new MenuItem(sett.allowStatus);
                var denyMenuItem = new MenuItem(sett.denyStatus);

                trayContextMenu.MenuItems.AddRange(new[] { allowMenuItem, denyMenuItem });

                allowMenuItem.Click += (sender, e) => SetStatus(true);

                denyMenuItem.Click += (sender, e) => SetStatus(false);

                tray.ContextMenu = trayContextMenu;

                tray.DoubleClick += (sender, e) => SetStatus(!status);

                tray.Visible = true;

                //remove tray icon when closing, otherwise it multiplies
                Closing += (sender, e) =>
                {
                    tray.Visible = false;
                    tray.Icon = null;
                    tray.Dispose();
                };
                #endregion

                #region scroll text
                if (sett.isScrollTextEnabled)
                {
                    try
                    {
                        var lines = File.ReadAllLines(sett.pathToLocalMediaFolder + "\\text.txt");
                        scrollText = lines[0];
                        StyleInit(scrollTextBlock, sett.scrollTextFont.Name, sett.scrollTextFont.Size, sett.statusMargin);
                        scrollTextBlock.Foreground = new SolidColorBrush(sett.statusFontColor);
                    }
                    catch (System.IO.FileNotFoundException)
                    {
                        scrollTextBlock.Visibility = Visibility.Collapsed;
                        logger.Error("text.txt is not found in {0}. disabling scroll text.", sett.pathToLocalMediaFolder);
                    }
                    catch (Exception e)
                    {
                        scrollTextBlock.Visibility = Visibility.Collapsed;
                        logger.Error("Unexpected error during scroll text init. {0} {1}", e.StackTrace, e.ToString());
                    }
                }
                else
                {
                    scrollTextBlock.Visibility = Visibility.Collapsed;
                }

                #endregion

                #region timer: handling media and main info window switch
                logger.Trace("Initializing timer...");
                timer.Tick += timer_Tick;
                timer.Interval = (int)sett.timeToShowInfo.TotalMilliseconds;

                #endregion

                #region media initializing
                logger.Trace("Initializing media...");
                //TODO: implement rouned media playback from folder +
                //TODO: implement syncing from remote folder to local one + 
                //--?> check case: deleting file from remote dir should syncronously delete it from local folder
                //TODO: playback on timeout +
                //TODO: settings in AD +?
                //TODO: scrolling text +

                string[] filesArray;
                if (Directory.Exists(sett.pathToLocalMediaFolder) && Directory.GetFiles(sett.pathToLocalMediaFolder).Length != 0)
                {
                    logger.Trace("Directory {0} , as stated in settings file, exists. Going to search for suitable files...");
                    filesArray = Directory.GetFiles(sett.pathToLocalMediaFolder);
                    Array.ForEach(filesArray, file => // add appropriate files to the playlist
                    {
                        var ext = Path.GetExtension(file);
                        if (allowedExtensions.Contains(ext))
                            mediaList.Add(new Uri(Path.GetFullPath(file)));
                    });
                    if (mediaList.Count != 0)
                    {
                        logger.Trace("Found {0} files to play. Starting player...", mediaList.Count);
                        timer.Enabled = true;
                    }
                    else
                    {
                        logger.Warn("Didn't found any appropriate file to play in {0}", sett.pathToLocalMediaFolder);
                    }
                }
                else
                {
                    logger.Warn("Media folder is either empty or doesn't exist at {0}", sett.pathToLocalMediaFolder);
                }

                mediaPlayer.MediaEnded += mediaPlayer_MediaEnded;

                currentMediaNumberInList = (currentMediaNumberInList + 1) % mediaList.Count;

                logger.Trace("Going to play media number {0} in list.", currentMediaNumberInList);

                mediaPlayer.Source = mediaList[currentMediaNumberInList];

                #endregion
            }
            catch (ArgumentOutOfRangeException e)
            {
                //System.Diagnostics.EventLog.WriteEntry(logSource, "App Warning: status overflow." + e);
                logger.Error("App Warning: status overflow. Stack trace:\n{0} {1}", e.StackTrace, e.ToString());
                System.Windows.MessageBox.Show("Произошла ошибка. Обратитесь к системному администратору.", "SVIP - Ошибка!");
                Application.Current.Shutdown();
            }
            catch (DivideByZeroException e)
            {
                logger.Error("Division by zero - no folder or... oh, shi! {0} {1}", e.StackTrace, e.ToString());
            }
            catch (Exception e)
            {
                //System.Diagnostics.EventLog.WriteEntry(logSource, "App Error: " + e + "\n something truly unexpected happened.");
                logger.Error("Truly bad error happened. {0} {1}", e.StackTrace, e.ToString());
                System.Windows.MessageBox.Show("Произошла чудовищная ошибка. Обратитесь к системному администратору.", "SVIP - Ошибка!");
                Application.Current.Shutdown();
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            logger.Trace(mediaModeOn ? "Time limit exceeded. Stopping media and starting timer..." : "Time limit exceeded. Stopping timer and starting media...");
            SetMedia(!mediaModeOn);
            
            //set secondary timer interval so to show video
            timer.Interval = timer.Interval == (int)sett.timeToShowInfo.TotalMilliseconds ? (int)sett.timeToShowVideo.TotalMilliseconds : (int)sett.timeToShowInfo.TotalMilliseconds;
        }

        void mediaPlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            logger.Trace("Media has ended. Selecting next media from list.");

            var oldMediaNumberInList = currentMediaNumberInList;

            currentMediaNumberInList = (currentMediaNumberInList + 1) % mediaList.Count;

            logger.Trace("Going to play media number {0} from {1}", currentMediaNumberInList, mediaList.Count);

            logger.Trace("oldNumber={0},currentNumber={1}",oldMediaNumberInList,currentMediaNumberInList);

            mediaPlayer.Source = mediaList[currentMediaNumberInList];

            if (oldMediaNumberInList == currentMediaNumberInList) 
                mediaPlayer.Stop();

            SetMedia(true);
        }

        private void StyleInit(System.Windows.Controls.TextBlock t, string fontName, double size, double margin)
        {
            t.FontFamily = new FontFamily(fontName);
            t.FontSize = size;
            t.Margin = new Thickness(0, margin, 0, 0);
        }

        private void SetMedia(bool _turnMediaOn)
        {
            logger.Trace("Changing media show state from {0} to {1}", mediaModeOn, _turnMediaOn);
            mediaModeOn = _turnMediaOn;
            if (_turnMediaOn)
            {
                logger.Trace("Turning media on.");

                innerStackPanel.Visibility = Visibility.Collapsed;
                mediaPlayer.Visibility = Visibility.Visible;

                MainWindow.Background = black;
                //statusTB.Opacity = 0.5;

                mediaPlayer.Volume = 0;
                mediaPlayer.Play();
            }
            else
            {
                logger.Trace("Turning media off");

                try
                {
                    mediaPlayer.Pause();
                }
                catch (InvalidOperationException e)
                {
                    System.Windows.MessageBox.Show("Pause operation failed.", "!");
                    logger.Error("Couldn't pause media due to {0} {1}", e.StackTrace, e.ToString());
                }

                mediaPlayer.Visibility = Visibility.Collapsed;
                innerStackPanel.Visibility = Visibility.Visible;

                MainWindow.Background = backgroundImage;
                //statusTB.Opacity = 1;
            }
        }



        //changes status to desired
        private void SetStatus(bool _status)
        {

            status = _status;

            statusText = status ? sett.allowStatus : sett.denyStatus;
            if (statusText.Length > 30) statusText = statusText.Substring(0, 30);

            //change status icon
            tray.Icon = status ? allowIcon : denyIcon;

            //change status msg
            string trayMsg;
            try
            {
                var nameSplit = name.Split(' ');
                trayMsg = nameSplit[0] + " " + nameSplit[1].Substring(0, 1) + "." + nameSplit[2].Substring(0, 1) + "." + "\n" + statusText;
            }

            catch (IndexOutOfRangeException)
            {
                logger.Debug("Couldn't split name by spaces to make a proper tray msg. Using whole name instead of it...");
                trayMsg = name;
            }

            tray.Text = trayMsg.Length <= 64 ? trayMsg : trayMsg.Substring(0, 64);
            statusTB.Text = statusText;

            //change background of status display
            statusTB.Background = new SolidColorBrush(status ? sett.allowColor : sett.denyColor);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            //split surname from other name parts if it overflows the screen
            if (ActualWidth <= nameTB.ActualWidth)
            {
                try
                {
                    var split = name.Split(' ');
                    nameTB.Text = split[0] + "\n" + split[1] + " " + split[2];

                }
                catch (IndexOutOfRangeException) { nameTB.Text = name; }
            }
            //prevent alt+tabbing to the application, see WindowsInteropExtensions.cs
            //http://stackoverflow.com/questions/357076/best-way-to-hide-a-window-from-the-alt-tab-program-switcher
            var wndHelper = new WindowInteropHelper(this);

            var exStyle = (int)WindowsInteropExtensions.GetWindowLong(wndHelper.Handle, (int)WindowsInteropExtensions.GetWindowLongFields.GWL_EXSTYLE);

            exStyle |= (int)WindowsInteropExtensions.ExtendedWindowStyles.WS_EX_TOOLWINDOW;
            WindowsInteropExtensions.SetWindowLong(wndHelper.Handle, (int)WindowsInteropExtensions.GetWindowLongFields.GWL_EXSTYLE, (IntPtr)exStyle);
        }

        private void SyncFolders(string remoteSourcePath, string localDestinationPath)
        {
            logger.Trace("Entered SyncFolders. remoteSourcePath: {0}, localDestinationPath: {1}");
            FileSyncProvider sourceProvider = null;
            FileSyncProvider destinationProvider = null;

            try
            {
                var allowedForSync = new List<string>(allowedExtensions);
                allowedForSync.Add(".txt");
                var wildcards = ((ICollection<string>)allowedForSync).Select(x => "*" + x);
                var filter = new FileSyncScopeFilter(new List<string>(), new List<string>(), 0, wildcards);

                sourceProvider = new FileSyncProvider(remoteSourcePath, filter, FileSyncOptions.None);
                logger.Trace("sourceProvider = {0}", sourceProvider.RootDirectoryPath);
                destinationProvider = new FileSyncProvider(localDestinationPath, filter, FileSyncOptions.RecycleConflictLoserFiles);
                logger.Trace("destinationProvider = {0}", destinationProvider.RootDirectoryPath);

                destinationProvider.AppliedChange += new EventHandler<AppliedChangeEventArgs>(OnAppliedChange);
                destinationProvider.SkippedChange += new EventHandler<SkippedChangeEventArgs>(OnSkippedChange);

                SyncOrchestrator agent = new SyncOrchestrator();
                agent.RemoteProvider = sourceProvider;
                agent.LocalProvider = destinationProvider;
                agent.Direction = SyncDirectionOrder.Download;

                logger.Info("Syncronizing from '{0}'... ", sourceProvider.RootDirectoryPath);

                agent.Synchronize();
            }
            catch (Exception e)
            {
                logger.Error("Something unpredicted has happened during file syncronisation. {0} {1}", e.StackTrace, e.ToString());
            }
            finally
            {
                // Release resources
                if (sourceProvider != null) sourceProvider.Dispose();
                if (destinationProvider != null) destinationProvider.Dispose();
            }
        }

        private void OnSkippedChange(object sender, SkippedChangeEventArgs args)
        {
            switch (args.SkipReason)
            {
                case SkipReason.ApplicationRequest:
                    logger.Info("Skipping file of improper format");
                    break;
                case SkipReason.ConflictLoserWriteError:
                    logger.Info("Name conflict. File {0} has not been written.", args.NewFilePath);
                    break;
                default:
                    logger.Warn("Skipped applying {0} for {1} due to error.", args.ChangeType,
                (!string.IsNullOrEmpty(args.CurrentFilePath) ? args.CurrentFilePath : args.NewFilePath));
                    break;
            }

        }

        private void OnAppliedChange(object sender, AppliedChangeEventArgs args)
        {
            switch (args.ChangeType)
            {
                case ChangeType.Create:
                    logger.Info("Applied CREATE for file {0}", args.NewFilePath);
                    break;
                case ChangeType.Delete:
                    logger.Info("Applied DELETE for file {0}", args.OldFilePath);
                    break;
                //case ChangeType.Overwrite:
                //    logger.Info("Applied OVERWRITE for file {0}", args.OldFilePath);
                //    break;
                case ChangeType.Rename:
                    logger.Info("Applied RENAME for file {0} as {1}", args.OldFilePath, args.NewFilePath);
                    break;
            }
        }

        #region bindings
        public string Username
        {
            get { return name; }
        }
        public string Position
        {
            get { return reception + position; }
        }
        public string Profession
        {
            get { return profession; }
        }
        public string JobTitle
        {
            get { return jobTitle; }
        }

        public string Greeting
        {
            get { return greeting; }
        }

        public string Status
        {
            get { return statusText; }
        }

        public string ScrollText
        {
            get { return scrollText; }
        }

        public Thickness NewMargin
        {
            get { return new Thickness(0, 0, 0, statusTB.Height); }
        }

        public Thickness NewMargin2
        {
            get { return new Thickness(0, 0, 0, statusTB.Height+50); }
        }
        #endregion
    }
}